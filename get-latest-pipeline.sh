#!/bin/bash

CURL="curl --silent"

if [ -z "${KERNEL_REPO_URL}" ]; then
  KERNEL_REPO_URL="https://gitlab.com/Linaro/lkft/mirrors/stable/linux-stable-rc.git"
fi
project="${KERNEL_REPO_URL#https://gitlab.com/*}"
project="${project%*.git}"

tag_series="v5.*.*"
if [ -n "${KERNEL_BRANCH}" ]; then
  # KERNEL_BRANCH=linux-5.15.y
  tag_series="v${KERNEL_BRANCH#linux-*}"
  tag_series="${tag_series%*.y}.*"
fi

# Look for most recent stable branch SHA
tags_txt="$(mktemp)"
rejected_tags_txt="$(mktemp)"
# shellcheck disable=SC2086
git ls-remote --tags --sort=v:refname \
  "${KERNEL_REPO_URL}" ${tag_series} > "${tags_txt}"
# Try until something is found
project_id="${project//\//%2F}"
project_url="https://gitlab.com/api/v4/projects/${project_id}"
pipeline_json="$(mktemp)"
while true; do
  last_revision="$(grep -E '\^\{\}$' "${tags_txt}" | grep -v -f "${rejected_tags_txt}" | tail -n1)"
  sha="$(echo "${last_revision}" | awk '{print $1}')"
  export KERNEL_SHA="${sha}"

  # Get pipeline for latest release from latest stable branch
  # shellcheck disable=SC2068
  ${CURL[@]} "${project_url}/pipelines?sha=${sha}" > "${pipeline_json}"
  pipeline_id="$(jq -r '.[] | select(.status != "canceled") | .id' "${pipeline_json}" | head -n1)"
  ref="$(jq -r '.[] | select(.status != "canceled") | .ref' "${pipeline_json}" | head -n1)"
  rm "${pipeline_json}"

  if [ -n "${pipeline_id}" ] && [ -n "${ref}" ]; then
    break
  else
    echo "${sha}" >> "${rejected_tags_txt}"
    unset KERNEL_SHA pipeline_id ref
  fi
done
rm "${tags_txt}" "${rejected_tags_txt}"


export LATEST_RELEASE_PIPELINE_ID="${pipeline_id}"

arg="--pipeline-id"
if [ $# -gt 0 ]; then
  arg="$1"
fi

if [ "${arg}" = "--pipeline-id" ]; then
  echo "${pipeline_id}"
elif [ "${arg}" = "--kernel-sha" ]; then
  echo "${sha}"
elif [ "${arg}" = "--kernel-branch" ]; then
  echo "${ref}"
fi
